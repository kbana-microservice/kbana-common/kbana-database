package com.kbana.postgres.repository;

import com.kbana.postgres.entity.KRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<KRole, Long> {
}
