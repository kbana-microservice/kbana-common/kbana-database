package com.kbana.postgres.repository;

import com.kbana.postgres.entity.KUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<KUser, Long>, JpaSpecificationExecutor<KUser> {

    KUser findByUsername(String username);
    KUser findByPhone(String phone);
}
