package com.kbana.postgres.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="kbana_role")
public class KRole {
    @Id
    @GeneratedValue(generator = "SEQ_ROLE")
    @SequenceGenerator(name = "SEQ_ROLE", sequenceName = "SEQ_ROLE", allocationSize = 1)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE", nullable = false, unique = true)
    private String code;

    @Column(name = "ROLE_NAME", nullable = false, unique = true)
    private String roleName;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "CREATE_DATE", insertable = false, updatable = false)
    private Timestamp createDate;

    @Column(name = "UPDATE_DATE")
    private Timestamp updateDate;

    @Column(name = "STATUS")
    private Integer status;

    @ManyToMany(mappedBy = "roles")
    private List<KUser> users;

    @ManyToMany
    @JoinTable(name = "KBANA_ROLE_PERMISSION",
            joinColumns = @JoinColumn(name = "ROLE_ID",
                    foreignKey = @ForeignKey(name = "K_ROLE_PERM_ROLE_FK")),
            inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID",
                    foreignKey = @ForeignKey(name = "K_ROLE_PERM_PERM_FK")))
    private List<KPermission> permissions;

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", roleName='" + roleName + '\'' +
                ", description='" + description + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", status=" + status +
                '}';
    }
}
