package com.kbana.postgres.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="kbana_user")
public class KUser {
    @Id
    @GeneratedValue(generator = "SEQ_USER")
    @SequenceGenerator(name = "SEQ_USER", sequenceName = "SEQ_USER", allocationSize = 1)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USERNAME", unique = true, nullable = false)
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "FULLNAME")
    private String fullname;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "PHONE", nullable = false)
    private String phone;

    @Column(name = "CREATE_DATE", insertable = false, updatable = false)
    private Timestamp createDate;

    @Column(name = "UPDATE_DATE")
    private Timestamp updateDate;

    @Column(name = "STATUS")
    private Long status;

    @Column(name = "LAST_CHANGE_PASSWORD")
    private Timestamp lastChangePassword;

    @ManyToMany
    @JoinTable(name = "kbana_user_role",
            joinColumns = @JoinColumn(name = "USER_ID",
                    foreignKey = @ForeignKey(name = "K_USER_ROLE_USER_FK")),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID",
                    foreignKey = @ForeignKey(name = "K_USER_ROLE_ROLE_FK")))
    private List<KRole> roles;

    @Override
    public String toString() {
        final String HIDE_PASSWORD = "*********";
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + HIDE_PASSWORD + '\'' +
                ", fullname='" + fullname + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", status=" + status +
                '}';
    }

}
